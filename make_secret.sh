#!/bin/bash
NAMESPACE=web
SECRET_NAME=scs-website

KEY1_NAME=mysql-pass
KEY1_SECRET=$(openssl rand -base64 20)

kubectl -n "$NAMESPACE" create secret generic "$SECRET_NAME" --from-literal="$KEY1_NAME"="$KEY1_SECRET"
kubectl -n "$NAMESPACE" get secret "$SECRET_NAME" -o yaml
